from random import randint

def creation_map(hauteur, longueur,pas, list, ecran):
    """"
    Fonction qui permet de creer une map en fonction des parametre
    hauteur : le nombre de ligne
    longeur : le nombre de colone
    pas: espacement des case
    ecran : la fenètre visuel que l'on modifie
    """
    hauteur*=pas
    longueur*=pas
    ###print(hauteur, longueur)
    for y in range(0, hauteur, pas):
        for x in range(0, longueur, pas):
            ###print(x,y)
            if list[y//pas][x//pas]==0:
                ecran.dessinerCercle(x+pas//2,y+pas//2,pas//2,"gray")
            elif list[y//pas][x//pas]>0:
                ecran.dessinerCercle(x+pas//2,y+pas//2,pas//2,"cyan")
            else:
                ecran.dessinerCercle(x+pas//2,y+pas//2,pas//2,"red")

    for y in range(0, hauteur, pas):
        for x in range(0, longueur, pas):
            ecran.afficherTexte(list[y // pas][x // pas], x + pas // 2, y + pas // 2,"white",pas//2)
            ecran.dessinerLigne(x, y, x, hauteur, 'white')
            ecran.dessinerLigne(x, y, longueur, y, 'white')

def grilleInit(ligne,col,max_malus,max_bonus):
    grille = [[0 for j in range(col)] for i in range(ligne)]
    for i in range(ligne):
        for j in range(col):
            grille[i][j] = randint(max_malus,max_bonus)
    return grille

def traversee_matrice_iteratif(grille,bottes):
    ###print("traversee_matrice_iteratif => bottes : " + str(bottes))
    
    resultat= [[0 for j in range(len(grille[0]))] for i in range(len(grille))]
    resultat[0][0] = grille[0][0]
    for j in range(1,len(grille[0])): #  parcours la première ligne 
        resultat[0][j] =  resultat[0][j-1] + grille[0][j] 
    for i in range(1,len(grille)):
        resultat[i][0] = resultat[i-1][0] + grille[i][0]
    for i in range(1,len(grille)):
        for j in range(1,len(grille[0])):
            if bottes:
                resultat[i][j] = max(resultat[i-1][j-1],resultat[i - 1][j], resultat[i][j - 1]) + grille[i][j]

                if resultat[i][j]==resultat[i-1][j-1]+grille[i][j]:
                    ###print("Botte utilisée, on ne peut plus le faire")
                    bottes = False
            else:
                ##print("on passe ici")
                resultat[i][j] = max(resultat[i - 1][j], resultat[i][j - 1]) + grille[i][j]
                
    return resultat

def afficher_plus_court_chemin(resultat, grille,pas, g,nb_potion, bottes):
    """
    Affiche le plus court de tout les chemin possible de la grille
    resultat: liste des valeurs de tout les chemin
    grille: liste du labirinte
    pas: nombre de
    g: fenetre de la grille
    """
    min=0
    sur_le_chemin = [[0 for j in range(len(grille[0]))] for i in range(len(grille))]
    chemin=[]
    peril_evité = [0 for j in range(nb_potion)]
    i=len(grille)-1
    j=len(grille[0])-1
    n = 0
    sur_le_chemin[i][j] = 1
    while i != 0  or j != 0:
        
        ##print("J : ",j)
        ##print(len(resultat))
        ##print(chemin)
        ##print(i, j, resultat[i][j], resultat[i-1][j-1]+grille[i][j], resultat[i-1][j]+grille[i][j], resultat[i][j-1]+grille[i][j])

        if i > 0 and j > 0:
            if bottes and resultat[i][j] == resultat[i-1][j-1]+grille[i][j]:
                ##print("Chemin botte")
                sur_le_chemin[i-1][j-1]=1
                
                chemin.append(grille[i][j])
                bottes = False
                i-=1
                j-=1
                
            elif resultat[i][j] == resultat[i-1][j]+grille[i][j]:
                sur_le_chemin[i-1][j]=1
                ##print("chemin haut")
                chemin.append(grille[i][j])
                i -= 1
            
            elif resultat[i][j] == resultat[i][j-1]+grille[i][j]:
                sur_le_chemin[i][j-1]=1
                ##print("chemin gauche")
                chemin.append(grille[i][j])
                j -= 1
                
            if resultat[i][j] < min:
                min=resultat[i][j]
                
        
        elif i == 0 and j > 0 and resultat[i][j] == resultat[i][j-1]+grille[i][j]:
            ##print("uniquement chemin gauche")
            sur_le_chemin[i][j-1]=1
            chemin.append(grille[i][j])
            j -= 1
            
            if resultat[i][j] < min:
                min = resultat[i][j]

           
            
        elif j == 0 and i > 0 and resultat[i][j] == resultat[i-1][j]+grille[i][j]:
            ##print("uniquement chemin haut")
            sur_le_chemin[i-1][j]=1
            
            chemin.append(grille[i][j])
            i=-1
            
            if resultat[i][j] < min:
                min=resultat[i][j]
            
            
        else:
            ###print("ici")
            continue
        """if ((j==0 and i >= 0) or (resultat[i][j]== resultat[i-1][j]+grille[i][j])):
            i=i-1
            if resultat[i][j] < min:
                min=resultat[i][j]
            chemin.append(grille[i][j])
        elif resultat[i][j]==resultat[i-1][j-1]+grille[i][j]:
            i-=1
            j-=1
            if resultat[i][j] < min:
                min=resultat[i][j]
            chemin.append(grille[i][j])
        else:
            j=j-1
            if resultat[i][j] < min:
                min = resultat[i][j]
            chemin.append(grille[i][j])"""
    ##print(chemin)
    ##print(resultat)
    for i in range(len(grille)):
        for j in range(len(grille[0])):
            if(sur_le_chemin[i][j]==1):
                ##print("point soluc : ", i, j)
                g.dessinerRectangle(j*pas,i*pas,pas,pas,"green")
    chemin.insert(0,grille[i][j])
    if(resultat[i][j]<min):
        min=resultat[i][j]

    for i in range(len(chemin)):
        for j in range (len(peril_evité)):
            if (peril_evité[j]>chemin[i]):
                peril_evité[j]=chemin[i]
    for i in range(len(peril_evité)):
        min+=abs(peril_evité[i])
    if min>0:
        return 0
    else:
        return min
