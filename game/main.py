from evironement.grille import *
from evironement.tkiteasy import *

import tkinter as tk
from tkinter import messagebox

pas = 25
bottes = False



def validation(l, h, d, m, p, flag = True ):
    
    l = int(l)
    h = int(h)
    d = int(d)
    m = int(m)
    p = int(p)
    
    if l <= 0 or l > largeur_max:
    
        messagebox.showinfo("ERREUR","Nombre de colonnes doit être compris entre 1 et " + str(largeur_max))
        flag = False
        
    if h <= 0 or h > hauteur_max:
    
        messagebox.showinfo("ERREUR","Nombre de colonnes doit être compris entre 1 et " + str(hauteur_max))
        flag = False
        
    if  d >= 0:
        
        messagebox.showinfo("ERREUR","Valeur des éprevues doit être négatif ")
        
        flag = False
    
    if m <= 0:
        
        messagebox.showinfo("ERREUR","Valeur max des sources doit être positif")
        
        flag = False
        
    if p < 0:
        messagebox.showinfo("ERREUR", "Valeur des potions superieures ou égales a 0")
        flag = False
    

    if flag == True :
   
        root.destroy()


root = tk.Tk()

hauteur_max = root.winfo_screenheight() // pas - 4
largeur_max = root.winfo_screenwidth() // pas

colVar,LiVar,epVar,soVar,poVar,boVar = tk.IntVar(),tk.IntVar(),tk.IntVar(),tk.IntVar(),tk.IntVar(),tk.IntVar()

# colVar.set(76)
# LiVar.set(39)
# epVar.set(-5)
# soVar.set(5)

colonneLabel = tk.Label(root, text="Nombre de colonnes doit être compris entre 1 et " + str(largeur_max) + " : ")
colonne = tk.Entry(root, textvariable=colVar)
colonneLabel.pack()
colonne.pack()

ligneLabel = tk.Label(root, text="Nombre de lignes doit être compris entre 1 et  " + str(hauteur_max) + " : ")
ligne = tk.Entry(root, textvariable=LiVar)
ligneLabel.pack()
ligne.pack()

epreuveLabel = tk.Label(root,text="Valeur des épreuves doit être négatif : " )
epreuve = tk.Entry(root, textvariable=epVar)
epreuveLabel.pack()
epreuve.pack()

sourceLabel = tk.Label(root, text="Valeur max des sources doit être positif : ")
source = tk.Entry(root, textvariable=soVar)
sourceLabel.pack()
source.pack()

nb_ligne = int(ligne.get())
nb_col= int(colonne.get())
malus_max = int(epreuve.get())
bonus_max = int(source.get())

potionLabel = tk.Label(root, text="Nombre de potions d'invisibilité du magicien")
potion = tk.Entry(root, textvariable=poVar)
potionLabel.pack()
potion.pack()

checkBottes= tk.Checkbutton(root, text="Activer les bottes",variable=boVar, onvalue = True, offvalue = False)
checkBottes.pack()

butonSumbmit = tk.Button(root, text="Valider", command=lambda:validation(colVar.get(),LiVar.get(),epVar.get(),soVar.get(),poVar.get()))
butonSumbmit.pack()

pas = 25
root.mainloop()

#print("ligne" ,int(LiVar.get()))
#print("colonne " ,int(colVar.get()))

lignes = int(LiVar.get())
colonnes = int(colVar.get())
etape  = int(epVar.get())
mana = int(soVar.get())

dep=grilleInit(lignes,colonnes,etape,mana)

#print("Botte activée ? " + str(int(boVar.get())))
if int(boVar.get()) == 1:
    bottes = True
else :
    bottes = False


#res = [[0 for j in range(nb_col)] for i in range(nb_ligne)]
res = traversee_matrice_iteratif(dep, bottes)

g=ouvrirFenetre(colonnes*pas,lignes*pas)

min =afficher_plus_court_chemin(res,dep,pas,g,int(poVar.get()), bottes)
#
# #print("min ",abs(min))
# #print("chemin ",chemin)

creation_map(lignes,colonnes,pas,dep,g)


solution = tk.Tk()
manaLabel = tk.Label(solution, text="le mana minimum dont le sorcier aura besoin est : " + str(abs(min)))
manaLabel.pack()

solution.mainloop()