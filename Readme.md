# L'epreuve des sorcier
---
## comment Lancer le programme

Pour lancer le programme il faut tout d'abord executer le fichier **main.py** qui se trouve dans le dossier **/game** 

une fois cela effectuer le programme ouvriras le menue permettant de choisir les dimension du lambyrinte.

une fois les champs remplie cliquer sur le bouton **suivant**.

pour fermer le programe fermer les fenêtre avec la crois en haut à droite.

## Les principales fonctionnaliter

 - le programme vas "scanner" la taille de l'ecrant affin de permettre a de proposer une taille respectant les dimmensions de l'ecrant affin d'avoir la totaliter du plateau sous les yeux

- les potion d'invisibiliter seront utiliser sur la case en valant le plus la peine (le plus gros malus rencontrer sur le chemin)

- les bottes permetrons s'il elle sont activer de faire un déplacement en diagonale

- une Popup s'ouvriras en même temps que le plateaux pour afficher le mana minimum dont le mage a besoins pour traverser le plateaux


## Expliquation de l'algorithmes
L'algorithme fonctionne de la manière suivante :

- Tout d'abord, le programme crée une grille aléatoire à partir des paramètres donnés par l'utilisateur.

- Ensuite, il calcule les chemins de la première ligne et de la première colonne en attribuant une valeur à chaque case.

- Puis, il attribue une valeur en additionnant la valeur de la case actuelle et le nombre le plus grand entre celui de la gauche et celui du haut.

- Après cela, le programme parcourt la grille en partant de la fin et retrace le chemin dans une fenêtre. Ensuite, la grille est créée visuellement sur la fenêtre tout en conservant le chemin et le minimum de mana nécessaire.

- Enfin, le programme affiche une deuxième fenêtre avec le chemin parcouru ainsi que le nombre de mana nécessaire pour compléter le labyrinthe.
