# Cahier des charges - L'épreuve des sorciers

## Introduction

Ce document traite de l'identification des exigences fonctionnelles, organisationnelles et technologiques concernant notre projet de création d'une simulation du jeu vidéo "L'épreuve des sorciers". Vous trouverez dans un premier temps la description du problème posé par le projet ainsi qu’un énoncé des exigences auxquelles celui-ci doit répondre. Ensuite, vous trouverez la liste des connaissances et technologies nécessaires à la réalisation du projet.

**Livraison du livrable :** À définir

*Légende :* Nous allons, dans ce document, utiliser des termes techniques relatifs au développement d'applications et au monde de la fantasy. Ainsi, en cas d'incompréhension, nous vous invitons à vous référer au glossaire introduit dans le document d'Analyse des besoins.

## Énoncé

Il nous a été demandé d'effectuer un projet de développement d'une simulation de jeu vidéo, ayant pour but de faire naviguer un sorcier sur un plateau. Chaque case aura un entier, soit négatif, soit positif. Si la jauge de mana est inférieure à 0 avant que le sorcier n'atteigne la fin du plateau, le jeu prend fin.

Le sorcier ne pourra se déplacer sur le plateau qu'en allant dans les cases situées sous lui ou à sa droite, à la vitesse d'une case par tour. Le sorcier commencera à la case (0,0).

Au début du jeu, le sorcier disposera d'une quantité de mana dans sa jauge, qui changera au cours du jeu en fonction des nombres sur lesquels le sorcier tombera au cours de son aventure. Dans un premier temps, il n'y aura pas de limite de temps pour parcourir le plateau. Pour finir, nous utiliserons le Python afin de développer la simulation.

## Les Nombres Positifs

Les nombres positifs influenceront la jauge de mana en la faisant grandir. Elle n'a pas de limite pour grandir et pourra donc grandir indéfiniment.

## Les Nombres Négatifs

Les nombres négatifs influenceront la jauge de mana en la faisant baisser, ce qui précipitera le magicien vers la fin de la partie si celle-ci est inférieure à 0. À noter que si la jauge de mana est égale à 0, cela ne compte pas comme une cause de perte de la partie.

## Les Conditions de Victoire

* Le jeu sera gagné si le sorcier atteint la case (n-1, m-1) avec sa jauge de mana supérieure ou égale à 0.
* La chute de la jauge de mana sous la barre des 0 avant que le magicien atteigne la case (n-1, m-1).

## Pré-Requis

Afin de mener à bien la réalisation de ce livrable, nous aurons besoin de développer le code en Python.

## Priorité

Il n’y a pas de priorité spécialement exprimée par le client.
