### Liste de cas d'utilisation :

| Cas d'utilisation n°1 : | Le client utilise l'application                                                                                                                                                                        |
|:------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Niveau :                | utilisateur                                                                                                                                                                                            |
| Précondition :          | avoir l'application                                                                                                                                                                                               |
| Etapes :                | - Le client execute le programme<br/><br/>- Le client selectionne les options de création de la grille sur le menue qui c'est ouvert<br/><br/>- Le client clique sur le bouton continuer<br/> <br/>- L'application s'execute est renvoie les résultats |

