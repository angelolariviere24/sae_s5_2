## Analyse des besoins


### Chapitre 1 – Objectif et portée

	 (a) Quels sont la portée et les objectifs généraux ?

L’objectif général de notre simulation est que le client puisse calculer l'évolution du personnage : 


* dans un plateaux dont on ne connais pas la taille

* avec des peripessies qui arriveront à chaque case

* avec un déplacement limiter a deux mouvement : en bas et a droite

Ainsi le client pour voir avec c'est trois paramètre l'evolution du personnage dans le plateau.


	 (b) Les intervenants. (Qui est concerné ?)

Seront amenés à intervenir sur ce projet les personnes suivantes : BOURION Théo, LARIVIERE Angelo, AUGER David, ainsi que le reste de l’équipe pédagogique de l’IUT de Vélizy.

Concernant le système de manière générale, l’étendue du projet se limitera à cet aspect : le client utilise la simulation, et optient le nombre minimale de mana qu'il faut pour traverser le plateau en suivant le chemin empreinter par le magicien. 



### Chapitre 2 – Terminologie employée / Glossaire

* mana : puissance magique du sorcier

* jauge de mana :  representation numerique de la puissance magique du sorcier

* plateau : matrice de **n** colone et de **m** ligne

* execute : fait fonctionner le programme en suivant les instructions noter dedans

### Chapitre 3 – Les cas d’utilisation


    (a) Les acteurs principaux et leurs objectifs généraux.

le client 

    (b) Les cas d’utilisation métier (concepts opérationnels).

le client lance la simulation et optien le mana minimale pour completer un plateaux de taille n , m


    (c) Les cas d’utilisation système.

le client execute le programme dans une console python.


* Le résultat est affiché dans la console python.


### Chapitre 4 – La technologie employée

    (a) Quelles sont les exigences technologiques pour ce système et avec quels systèmes ce système s’interfacera t-il et avec
        quelles exigences ?



notre simulation seras developper sous python et fonctionneras sur les pc de l'iut.


### Chapitre 5 – Autres exigences

Afin de mener à bien la réalisation du livrable, nous opérons selon un cycle court de la manière suivante:


* Analyse des besoins : document d’analyse des besoins (que vous lisez actuellement)


* Spécifications : rédaction de cas d’utilisations.

* Programmation : création de la simulation par partie importante ( deplacement , condition de victoire)

* Tests : des partie important puis l'ensemble



    (b) Règles métier



    (c) Performances


###### Module 1 :

Il faudrait que le module n'affiche pas un résultat inverse à la requête PING indiquée. Exp : si le ping n'est pas réussi,
qu'il n'indique pas qu'il a réussi.


###### Module 2 :

Il faudrait que le module n'affiche pas un résultat différent des résultats attendus pour les conversions.
Il faudrait que pas que les valeurs pour les bits de points fort soient fausses.



###### Module 3 :

Il faudrait que le module n'affiche pas un résultat incohérent quant à si le(s) nombre(s) de machines peuvent être mis sur 
le réseau ou non.


    (d) Opérations, sécurité, documentation


Opérations : requêtes PING, conversion d'adresses, division de sous réseaux

Sécurité : Pas de mesure spéciale prise, selon la demande du client

Documentation : Cahier des Charges, Analyse des besoins, Gantt, Spécifications/Cas d'utilisation


    (e) Utilisation et utilisabilité

Utilisation par les clients

    (f) Maintenance et portabilité

Portabilité : portabilité assurée par l'utilisation d'une archive tar.gz

Maintenance : Facilement maintenable car nous pouvons le travailler de n'importe où (grâce à l'utilisation de git).

    (g) Questions non résolues ou reportées à plus tard

(à remplir au fur et à mesure)


### VI./ Chapitre 6 – Recours humain, questions juridiques, politiques, organisationnelles.


    (a) Quel est le recours humain au fonctionnement du système ?

L'application sera réalisé par une équipe de 2 personnes. La documentation sera majoritairement rédigée par une à deux personnes et
la mise en place de la simulation sera configurée par 2 personnes.

    (b) Quelles sont les exigences juridiques et politiques ?

Aucune pour un projet de ce type.

    (c) Quelles sont les conséquences humaines de la réalisation du système ?

Aucune pour un projet de ce type.

    (d) Quels sont les besoins en formation ?


Aucun pour un projet de ce type.

    (e) Quelles sont les hypothèses et les dépendances affectant l’environnement humain ?

Nous devons travailler en salles dédiées au travail à l’IUT de Vélizy.
